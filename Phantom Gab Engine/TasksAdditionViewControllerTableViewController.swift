//
//  TasksAdditionViewControllerTableViewController.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 08/07/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//

import UIKit

class TasksAdditionViewControllerTableViewController: UITableViewController {
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "unwindToCreation", sender: sender)
    }

    
    var sections = [Section]()
    var apiEndpoint = "https://nameless-meadow-44269.herokuapp.com/phantom/getEmployees"
    var selectedEmployees: [ RolesTableViewController.SelectedEmployee ]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sections = []
        self.tableView.allowsMultipleSelection = true
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Tasks"
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let frame: CGRect = tableView.frame
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        let addButton = UIButton(frame: CGRect(x: frame.maxX - 30, y: tableView.rowHeight / 4, width: 20, height: 20))
        let sectionLabel = UILabel(frame: CGRect(x: frame.minX + 10, y: tableView.rowHeight / 4, width: 0, height: 0))
        sectionLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        sectionLabel.text = "Tasks"
        sectionLabel.sizeToFit()
        addButton.setTitle("+", for: .normal)
        addButton.setTitleColor(UIColor.black, for: .normal)
        addButton.addTarget(self, action: #selector(TasksAdditionViewControllerTableViewController.createNewTask), for: .touchUpInside)
        headerView.addSubview(addButton)
        headerView.addSubview(sectionLabel)
        return headerView
    }
    
    @objc private func createNewTask(sender: UIButton) {
        let alert = UIAlertController(title: "Add new Task",
                                      message: "Please create a new task",
                                      preferredStyle: .alert)
        alert.addTextField{ taskNameText in
            taskNameText.placeholder = "Enter task name"
        }
        
        let createAction = UIAlertAction(title: "Create",
                                         style: .default) { action in
            let taskNameTextField = alert.textFields![0]
            self.sections.append(Section(name: taskNameTextField.text!, items: []))
            self.tableView.reloadData()
        }
        let dismissAction = UIAlertAction(title: "Cancel",
                                         style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
        }

        alert.addAction(createAction)
        alert.addAction(dismissAction)
        present(alert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = sections.count
        for section in sections {
            count += section.items.count
        }
        return count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = getSectionIndex(row: indexPath.row)
        let row = getRowIndex(row: indexPath.row)
        
            // Header has fixed height
        if row == 0 {
            return 50.0
        }
        return sections[section].collapsed! ? 0 : 44.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Calculate the real section index and row index
        let section = getSectionIndex(row: indexPath.row)
        let row = getRowIndex(row: indexPath.row)
        
        if row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderCell
            cell.titleLabel.text = sections[section].name
            cell.toggleButton.tag = section
            cell.toggleButton.setTitle(sections[section].collapsed! ? "+" : "-", for: .normal)
            cell.toggleButton.addTarget(self, action: #selector(TasksAdditionViewControllerTableViewController.toggleCollapse), for: .touchUpInside)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "row") as UITableViewCell!
            cell?.textLabel?.text = sections[section].items[row - 1]
            return cell!
        }
    }
    
    
    
    
    /*
    func toggleCollapseForRoles(sender: UIButton) {
        let row_index = sender.tag
        let collapsed = roles[row_index].collapsed
        
        // Toggle collapse
        roles[row_index].collapsed = !collapsed!
        
        let indices = getHeaderIndices(sections:roles)
        
        let start = indices[row_index]
        let end = start + roles[row_index].items.count
        
        tableView.beginUpdates()
        for i in start ..< end + 1 {
            tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
        }
        tableView.endUpdates()
    }
    */

    
    func toggleCollapse(sender: UIButton) {
        let section = sender.tag
        let collapsed = sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = !collapsed!
        
        let indices = getHeaderIndices()
        
        let start = indices[section]
        let end = start + sections[section].items.count
        
        tableView.beginUpdates()
        for i in start ..< end + 1 {
            tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
        }
        tableView.endUpdates()
    }
    
    func getSectionIndex(row: Int) -> Int {
        let indices = getHeaderIndices()
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                return i
            }
        }
        return -1
    }
    
    func getRowIndex(row: Int) -> Int {
        var index = row
        let indices = getHeaderIndices()
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                index -= indices[i]
                break
            }
        }
        return index
    }
    
    func getHeaderIndices() -> [Int] {
        var index = 0
        var indices: [Int] = []
        for section in sections {
            indices.append(index)
            index += section.items.count + 1
        }
        
        return indices
    }
    
    
    
    struct Section {
        var name: String!
        var items: [String]!
        var collapsed: Bool!
        
        init(name: String, items: [String], collapsed: Bool = true) {
            self.name = name
            self.items = items
            self.collapsed = collapsed
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("At selection\(indexPath.row)")
        
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("At deselection\(indexPath.row)")
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
   */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let creatorViewController = segue.destination as! GroupCreatorViewController
        creatorViewController.selectedEmployees = selectedEmployees
        creatorViewController.selectedTasks = sections
        creatorViewController.abc()
    }
}

