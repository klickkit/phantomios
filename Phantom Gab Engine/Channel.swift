

import Foundation

internal class Channel {
    internal let id: String
    internal let name: String
    internal var dpImageData: Data?
    var userIds : [String] = []
    
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    func hasClickedCreatedChannel(newChannelName : String) -> Bool {
        return self.name == newChannelName
    }
    
    func addDpImageData(imageData: Data) {
        self.dpImageData = imageData
    }
    
}





