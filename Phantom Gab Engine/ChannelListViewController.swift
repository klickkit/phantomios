//
//  ChannelListViewController.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 5/27/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//
import Firebase
import UIKit

class ChannelListViewController: UITableViewController{
    
    
    //MARK: Properties
    
    var isNewChannelCreated: Bool = false
    private var newChannelName: String?
    private var channels: [Channel] = []
    private static let userEmail = Auth.auth().currentUser?.email
    private var dotStrippedEmail: String = ChannelListViewController.setDotStrippedEmail(email: userEmail!)
    private lazy var masterRef: DatabaseReference = Database.database().reference().child("master")
    private lazy var userRef: DatabaseReference = self.setUserReference()
    private lazy var channelRef: DatabaseReference = self.setChannelReference()
    lazy var storageRef: StorageReference = Storage.storage().reference(forURL: "gs://phantom-gab-engine.appspot.com/")
    private var userRefHandle: DatabaseHandle?
    private var haveToDoConfiguration: Bool = false
    var senderDisplayName: String?
    
    private static func setDotStrippedEmail(email: String) -> String {
        let atStrippedEmail = email.replacingOccurrences(of: "@", with: "", options: .literal, range: nil)
        let strippedEmail = atStrippedEmail.replacingOccurrences(of: ".", with: "", options: .literal, range: nil)
        return strippedEmail
    }
    
    private func setUserReference() -> DatabaseReference {
        userRef = masterRef.child(dotStrippedEmail).child("associated_rooms")
        return userRef
    }
    
    private func setChannelReference() -> DatabaseReference {
        channelRef = masterRef.child("channels")
        return channelRef
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        observeChannels()
        tableView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var arrayIndex = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "channelName", for: indexPath) as! ChannelListCell
        cell.channelName.text = channels[arrayIndex].name
        if let dpImageData = channels[arrayIndex].dpImageData {
            let dpImage = UIImage(data: dpImageData)!
            let xmin = cell.imView.frame.minX
            let ymin = cell.imView.frame.minY
            let frame = CGRect(x: xmin, y: ymin, width: 40, height: 40)
            cell.imView.frame = frame
            cell.imView.layer.cornerRadius = cell.imView.frame.size.width / 2
            cell.imView.clipsToBounds = true
            let width = cell.imView.frame.width
            print("The width is \(width)")
            let height = cell.imView.frame.height
            let size = CGSize(width: width, height: height)
            let properImage = scaleUIImageToSize(image: dpImage, size: size)
            cell.imView.image = properImage
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = channels[indexPath.row]
        if isNewChannelCreated {
            var hasSelectedCreatedChannel = channel.hasClickedCreatedChannel(newChannelName: newChannelName!)
            if hasSelectedCreatedChannel {
                isNewChannelCreated = false
                hasSelectedCreatedChannel = false
                self.performSegue(withIdentifier: "configureChannel", sender: channel)
            }
        }
        self.performSegue(withIdentifier: "showChannel", sender: channel)
    }
    
    
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "Add Channel",
                                      message: "Please create a Channel",
                                      preferredStyle: .alert)
        alert.addTextField{ textChannelName in
            textChannelName.placeholder = "Enter Channel name"
        }
        
        let createAction = UIAlertAction(title: "Create",
                                         style: .default) { action in
            let channelNameTextField = alert.textFields![0]
            if let channelName = channelNameTextField.text {
                let newuserRef = self.userRef.childByAutoId()
                let channelItem = [
                    "name" : channelName
                ]
                newuserRef.setValue(channelItem)
                self.isNewChannelCreated = true
                self.newChannelName = channelName
            }
            
                                            
        }
        
        alert.addAction(createAction)
        present(alert, animated: true, completion: nil)
    }
    
    public func addChannel(channelName: String, channelImage: Data, employees: [String]) {
        print("Im here")
        let filePath = "\(channelName)/dp"
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        print("Im just before this")
        let email = Auth.auth().currentUser?.email
        
        
        self.storageRef.child(filePath).putData(channelImage, metadata: metaData, completion: {(metaData, error) in
            print("Im into pushing")
            if let error = error {
                print("Im at the error")
                print(error.localizedDescription)
                return
            }
            else {
                print("Successfull")
                let downloadURL = metaData!.downloadURL()!.absoluteString
                let newChannelRef = self.channelRef.childByAutoId()
                let pushID = newChannelRef.key
                let channelItem = [
                    "name" : channelName,
                    "dpURL": downloadURL
                ]
                newChannelRef.setValue(channelItem)
                var tempRef: DatabaseReference = self.masterRef
                print(employees)
                if !employees.isEmpty {
                    for email in employees {
                        let strippedOutEmail = ChannelListViewController.setDotStrippedEmail(email: email)
                        tempRef = self.masterRef.child(strippedOutEmail).child("associated_rooms").childByAutoId()
                        let channelDetail = [
                            "channel_id" : pushID
                        ]
                        tempRef.setValue(channelDetail)
                    }
                }
            }
        })
    }
    
    func getRequest(url: URL, channelId: String) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            print("Ive communicated with the API")
            if let imageData = data as? Data {
               print(imageData)
                for channel in self.channels {
                    if channelId == channel.id {
                        print("Im here")
                        channel.addDpImageData(imageData: imageData)
                        self.tableView.reloadData()
                    }
                }
            }
        })
        task.resume()
    }

    
    private func observeChannels() {
        
        userRefHandle = userRef.observe(.childAdded, with: { (snapshot) -> Void in
            print("Im here")
            let associatedRoomData = snapshot.value as! Dictionary<String, AnyObject>
            let channelId = associatedRoomData["channel_id"] as! String
            self.channelRef.child(channelId).observeSingleEvent(of: .value, with: { (snapshot) in
                print(snapshot.value)
                print("Im in here")
                let channelData = snapshot.value as? Dictionary<String, AnyObject>
                print("The resolved value is \(channelData)")
                if let channelData = channelData {
                    let name = channelData["name"] as! String!
                    print(name)
                    self.channels.append(Channel(id: channelId, name: name!))
                    print(self.channels)
                    self.tableView.reloadData()
                    if let dpURL = channelData["dpURL"] as? String {
                        let dpURL = URL(string: dpURL)
                        if dpURL != nil {
                            self.getRequest(url: dpURL!, channelId: channelId)
                        }
                    }
                }
            })
        })
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    // MARK: - Navigation

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let channel = sender as? Channel {
            if segue.destination is ChatViewController {
                let chatVc = segue.destination as! ChatViewController
                chatVc.channel = channel
                chatVc.senderDisplayName = senderDisplayName
                chatVc.channelRef = channelRef.child(channel.id)
            }
            else {
                let navigationVc = segue.destination as! UINavigationController
                let channelConfigVC =
                    navigationVc.viewControllers.first as! ChannelConfigurationTableViewController
                channelConfigVC.channelName = channel.name
            }
        }
    }
    
    func scaleUIImageToSize( image: UIImage, size: CGSize) -> UIImage {
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }

 

}
