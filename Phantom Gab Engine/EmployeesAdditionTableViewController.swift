//
//  EmployeesAdditionTableViewController.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 07/07/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//

import Firebase
import UIKit

class EmployeesAdditionTableViewController: UITableViewController {

    
    var employeeReference: DatabaseReference?
    var employeeHandle: DatabaseHandle?
    var employeesMapping: [ [String:String] ] = [ [:] ]
    var selectedEmployees: [ [String:String] ] = [ [:] ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        employeeReference = Database.database().reference().child("employees")
        observeEmployees()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    private func observeEmployees() {
        employeeHandle = employeeReference?.observe(.childAdded, with: { (snapshot) -> Void in
            let employeeData = snapshot.value as! Dictionary<String, String>
            if let name = employeeData["name"] as String!, let email = employeeData["email"] as String!  {
                self.employeesMapping.append(["name" : name, "email" : email])
                self.tableView.reloadData()
            }
            else {
                print("Error, Could not decode message data")
            }
        })

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return employeesMapping.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let employeeName = employeesMapping[indexPath.row]["name"]
        let cell = tableView.dequeueReusableCell(withIdentifier: "employeeCell", for: indexPath)
        cell.textLabel?.text = employeeName
        return cell
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if (cell?.accessoryType == .checkmark) {
            cell?.accessoryType = .none
        }
        let employeeObj = employeesMapping[indexPath.row]
        cell?.accessoryType = .checkmark
    }
   
}
