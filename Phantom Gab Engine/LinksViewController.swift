//
//  LinksViewController.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 29/07/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//

import UIKit

class LinksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    let linkRequestURL: URL = URL(string: "http://localhost:8000/phantom/listLinks/hackathon")!
    
    
    var linkList: [[String : String]] = [[:]]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getRequest(url: linkRequestURL)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("")
        let cell = tableView.dequeueReusableCell(withIdentifier: "showLinks")
        let frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 44)
        cell?.frame = frame
        let linkDict = linkList[indexPath.row]
        cell?.textLabel?.text = linkDict["linkName"]
        let urlString = linkDict["url"]
        cell?.detailTextLabel?.text = urlString
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return linkList.count
    }
    
    func getRequest(url: URL) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            print("Ive communicated with the API")
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            
            if let responseJSON = responseJSON as? [[String: String]] {
                for jsonDict in responseJSON {
                    let linkName = jsonDict["link_name"] as! String
                    let url = jsonDict["url"] as! String
                    let linkDict = ["linkName" : linkName, "url" : url]
                    print(linkDict)
                    self.linkList.append(linkDict)
                    self.tableView.reloadData()
                }
            }
        })
        task.resume()
        
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
