//
//  Employee.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 21/07/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//

import Foundation

internal class Employee {
    
    internal let name: String
    internal let email: String
    internal var dpImageData: Data?
    
    
    init(name: String, email: String) {
        
        self.name = name
        self.email = email
    
    }
    
    func addDpImageData(imageData: Data) {
        self.dpImageData = imageData
    }
    
}

