//
//  TasksAdditionViewControllerTableViewController.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 08/07/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//

import UIKit

class RolesTableViewController: UITableViewController {
    
    
    var sections: [Section] = []
    var apiEndpoint = "http://localhost:8000/phantom/getEmployees"
    var selectedEmployees =  [[Int : SelectedEmployee]]()
    
    var apiCallIterable = ["Android Developer" : "AD", "iOS Developer" : "ID", "Web Frontend Developer" : "WFD",
                          "Backend Developer" : "BD", "Designer" : "DS"]
    var apiArray = ["Android Developer", "iOS Developer", "Web Frontend Developer",
                    "Backend Developer", "Designer"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.allowsMultipleSelection = true
        
        for element in apiArray {
            var verbose = element
            var tempApiEndPoint = apiEndpoint + "/" + apiCallIterable[element]!
            print(tempApiEndPoint)
            let getURL = URL(string: tempApiEndPoint)
            getRequest(url: getURL!, verbose: verbose)
            print(sections)
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Roles"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = sections.count
        for section in sections {
            count += section.items.count
        }
        return count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = getSectionIndex(row: indexPath.row)
        let row = getRowIndex(row: indexPath.row)
        
        // Header has fixed height
        if row == 0 {
            return 50.0
        }
        return sections[section].collapsed! ? 0 : 44.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Calculate the real section index and row index
        let section = getSectionIndex(row: indexPath.row)
        let row = getRowIndex(row: indexPath.row)
        
        if row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderCell
            cell.titleLabel.text = sections[section].name
            cell.toggleButton.tag = section
            cell.toggleButton.setTitle(sections[section].collapsed! ? "+" : "-", for: .normal)
            cell.toggleButton.addTarget(self, action: #selector(TasksAdditionViewControllerTableViewController.toggleCollapse), for: .touchUpInside)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "developerCell") as! DeveloperCell
            cell.nameLabel.text = sections[section].items[row - 1]["name"]
            cell.magnitudeLabel.text = sections[section].items[row - 1]["priority"]
            return cell
        }
    }
    
    /*
     func toggleCollapseForRoles(sender: UIButton) {
     let row_index = sender.tag
     let collapsed = roles[row_index].collapsed
     
     // Toggle collapse
     roles[row_index].collapsed = !collapsed!
     
     let indices = getHeaderIndices(sections:roles)
     
     let start = indices[row_index]
     let end = start + roles[row_index].items.count
     
     tableView.beginUpdates()
     for i in start ..< end + 1 {
     tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
     }
     tableView.endUpdates()
     }
     */
    
    
    func toggleCollapse(sender: UIButton) {
        let section = sender.tag
        let collapsed = sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = !collapsed!
        
        let indices = getHeaderIndices()
        
        let start = indices[section]
        let end = start + sections[section].items.count
        
        tableView.beginUpdates()
        for i in start ..< end + 1 {
            tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
        }
        tableView.endUpdates()
    }
    
    func getSectionIndex(row: Int) -> Int {
        let indices = getHeaderIndices()
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                return i
            }
        }
        return -1
    }
    
    func getRowIndex(row: Int) -> Int {
        var index = row
        let indices = getHeaderIndices()
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                index -= indices[i]
                break
            }
        }
        return index
    }
    
    
    
    func getHeaderIndices() -> [Int] {
        var index = 0
        var indices: [Int] = []
        for section in sections {
            indices.append(index)
            index += section.items.count + 1
        }
        
        return indices
    }
    
    
    
    struct Section {
        var name: String!
        var items: [[String : String]]!
        var collapsed: Bool!
        
        init(name: String, items: [[String : String]], collapsed: Bool = true) {
            self.name = name
            self.items = items
            self.collapsed = collapsed
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = getSectionIndex(row: indexPath.row)
        let row = getRowIndex(row: indexPath.row)
        let cell = tableView.cellForRow(at: indexPath)
        
        if row != 0 {
            cell?.accessoryType = .checkmark
            var theEmployee = sections[section].items[row - 1]
            var theRole = apiCallIterable[sections[section].name]
            self.selectedEmployees.append([indexPath.row : SelectedEmployee(role_name: theRole!, item: theEmployee)])
            print(selectedEmployees)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        print(selectedEmployees)
        print("Yes")
        if (selectedEmployees != nil) {
            for (index,employee) in selectedEmployees.enumerated() {
                for (rowIndex, emp) in employee {
                    if (rowIndex == indexPath.row) {
                        cell?.accessoryType = .none
                        self.selectedEmployees.remove(at: index)
                        print(selectedEmployees)
                    }
                }
            }
        }
    }
    
    func getRequest(url: URL, verbose: String) {
        sections.removeAll()
        var section = Section(name: verbose, items: [])
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            print("Ive communicated with the API")
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [[String: Any]] {
                for jsonDict in responseJSON {
                    let userDict = jsonDict["user"] as! [String:String]
                    let name = userDict["username"] as! String
                    let email = userDict["email"] as! String
                    let priorityList = jsonDict["priority"] as! [[String: Int]]
                    for priorityDict in priorityList {
                        let magnitude = priorityDict["magnitude"] as! Int
                        DispatchQueue.main.sync {
                            var itemDict = ["name": name, "email": email, "priority" : String(magnitude)]
                            section.items.append(itemDict)
                        }
                    }
                    
                }
                self.sections.append(section)
                self.tableView.reloadData()
            }
        })
        task.resume()
        
    }
    
    
    struct SelectedEmployee {
        var role_name: String;
        var item: [String:String]
        
        init(role_name: String, item: [String: String]) {
            self.role_name = role_name
            self.item = item
        }
    }
    
   /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let taskViewController = segue.destination as! TasksAdditionViewControllerTableViewController
        var returnableSelectedEmployees: [SelectedEmployee] = []
        for selectedEmployeeDict in selectedEmployees {
            for (key, value) in selectedEmployeeDict {
                returnableSelectedEmployees.append(value)
            }
        }
        taskViewController.selectedEmployees = returnableSelectedEmployees
    }
 
    
    
}
