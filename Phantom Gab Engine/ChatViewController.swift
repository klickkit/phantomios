//
//  ChatViewController.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 5/29/17.
//  Copyright © 2017 Klickkit Inc. All rights reserved.
//

import Firebase
import JSQMessagesViewController
import UIKit

class ChatViewController: JSQMessagesViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Firebase properties
    
    var imageURL = Auth.auth().currentUser?.photoURL
    var channelListConfig: ChannelConfigurationTableViewController?
    var channelRef: DatabaseReference?
    private lazy var messageRef: DatabaseReference = self.channelRef!.child("messages")
    private var newMessageRefHandle: DatabaseHandle?
    var haveToDoConfiguration: Bool?
    let stringPatternToMatch = "@Phantom"
    var apiMessageDict = [String : String]()
    let messageSubmissionURL = URL(string: "http://localhost:8000/phantom/submitMessage")
    let projectIncludedEmployeesURL = URL(string:"http://localhost:8000/phantom/getProjectEmployees/WAR")
    var employeeTableView: UITableView = UITableView()
    var employees: [Employee] = []
    var selectedEmployee: String? {
        didSet {
            if (employeeRange != nil) {
                if (whenIgenerateSS) {
                    myMessageTextView?.text.removeSubrange(employeeRange!)
                }
            }
            myMessageTextView?.text = (myMessageTextView?.text)! + selectedEmployee!
            employeeTableView.isHidden = true
            self.employees.removeAll()
            reloadEmployees()
            employeeTableView.reloadData()
        }
    }
    var whenIgenerateSS: Bool = false
    var isEmployeeString: Bool = false
    var optimalAtIndex: String.Index? = nil
    var myMessageTextView: UITextView?
    var employeeBeginIndices: [String.Index] = []
    var spaceIndices: [String.Index] = []
    var subStringToBeComparedWithEmployees : String = ""
    var substituteArray: [Employee] = []
    var employeeRange: Range<String.Index>!
    var currentUserEmail: String?
    var atIndices: [String.Index] = []
    
    //MARK: Local properties
    
    var channel: Channel? {
        didSet {
            title = channel?.name
        }
    }
    
    var messages = [JSQMessage]()
    
    private lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    private lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    
    private lazy var userIsTypingRef: DatabaseReference =
        self.channelRef!.child("typingIndicator").child(self.senderId) // 1
    private var localTyping = false // 2
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            // 3
            localTyping = newValue
            userIsTypingRef.setValue(newValue)
        }
    }
    
    
    private lazy var usersTypingQuery: DatabaseQuery =
        self.channelRef!.child("typingIndicator").queryOrderedByValue().queryEqual(toValue: true)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.senderId = Auth.auth().currentUser?.uid
        self.currentUserEmail = Auth.auth().currentUser?.email
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        configureEmployeeRecommender()
        observeMessages()
        substituteArray = employees
        print(imageURL)
        getRequest(url: projectIncludedEmployeesURL!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        observeTyping()
    }
    
    
    private func observeTyping() {
        let typingIndicatorRef = channelRef!.child("typingIndicator")
        userIsTypingRef = typingIndicatorRef.child(senderId)
        userIsTypingRef.onDisconnectRemoveValue()
        usersTypingQuery.observe(.value) { (data: DataSnapshot) in
            // 2 You're the only one typing, don't show the indicator
            if data.childrenCount == 1 && self.isTyping {
                return
            }
            
            // 3 Are there others typing?
            self.showTypingIndicator = data.childrenCount > 0
            self.scrollToBottom(animated: true)
        }

    }
    
    private func reloadEmployees() {
        employees.removeAll()
        for employee in substituteArray {
            employees.append(employee)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        var message = self.messages[indexPath.item]
        if message.senderId == self.senderId {
            return nil
        }
        
        if indexPath.item > 0 {
            var previousMessage = self.messages[indexPath.item - 1]
            if previousMessage.senderId == message.senderId {
                return nil
            }
        }
        return NSAttributedString(string: message.senderDisplayName)
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        var message = self.messages[indexPath.item]
        if message.senderId == self.senderId {
            return 0
        }
        
        if indexPath.item > 0 {
            var previousMessage = self.messages[indexPath.item - 1]
            if previousMessage.senderId == message.senderId {
                return 0
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        if indexPath.item % 3 == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        return 0
    }
    
    
    
    private func configureEmployeeRecommender() {
        let frame = CGRect()
        employeeTableView = UITableView(frame: frame ,style: UITableViewStyle.plain)
        employeeTableView.layer.cornerRadius = 10.0
        employeeTableView.delegate = self
        employeeTableView.dataSource = self
        employeeTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(employeeTableView)
        employeeTableView.translatesAutoresizingMaskIntoConstraints = false
        employeeTableView.isHidden = true

    }
    
    private func findSubStringsInEmployees(subString: String) {
        employees.removeAll()
        for employee in substituteArray {
           var resultBoolean = employee.name.ranges(of: subString)
            if (resultBoolean) {
                print(employee)
                print(employees)
                employees.append(employee)
                employeeTableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Collection view data source and methods
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleRed())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        }
        else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let message = messages[indexPath.item]
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
        }
        else {
            cell.textView?.textColor = UIColor.black
        }
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        if indexPath.item % 3 == 0 {
            var message = self.messages[indexPath.item]
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        return nil;
    }
    private func convertToJSON(toConvertData: Any) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: toConvertData, options: .prettyPrinted)
            submitToApi(url: messageSubmissionURL!, jsonData: jsonData)
        }
        catch {
            print ("Error converting to json")
        }
 
    }
    
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        isTyping = false
        var strippedOutText = text.trimmingCharacters(in: .whitespaces)
        print(strippedOutText)
        let startIndex = strippedOutText.startIndex
        if (strippedOutText.characters.count >= 8) {
            let endIndex = strippedOutText.index(startIndex, offsetBy: 8)
            let subString = strippedOutText.substring(to: endIndex)
            if subString == "@phantom" {
            apiMessageDict["message"] = text
            apiMessageDict["channel_name"] = channel?.name
                if currentUserEmail != nil {
                    apiMessageDict["email"] = currentUserEmail!
                }
            convertToJSON(toConvertData: apiMessageDict)
            }
        }
        let itemRef = messageRef.childByAutoId()
        let messageItem = [
            "senderId" : senderId!,
            "senderName" : senderDisplayName!,
            "text" : text!
        ]
        itemRef.setValue(messageItem)
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        finishSendingMessage()
    }
    
    private func observeMessages() {
       let messageQuery = messageRef.queryLimited(toLast: 25)
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData = snapshot.value as! Dictionary<String, String>
            if let id = messageData["senderId"] as String!, let name = messageData["senderName"] as String!, let text = messageData["text"] as String!, text.characters.count > 0  {
                self.addMessage(withId: id, name: name, text: text)
                self.finishReceivingMessage()
            }
            else {
                print("Error, Could not decode message data")
            }
            
        })
    }
    
    private func addMessage(withId id: String, name: String, text: String) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text) {
            messages.append(message)
            collectionView.reloadData()
        }
    }
    
    func submitToApi(url: URL, jsonData: Data) {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            print("Ive communicated with the API")
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                var speechResponse = responseJSON["speech_response"] as! String
                DispatchQueue.main.sync {
                    let itemRef = self.messageRef.childByAutoId()
                    let messageItem = [
                        "senderId" : "phantom",
                        "senderName" : "phantom",
                        "text" : speechResponse
                    ]
                    itemRef.setValue(messageItem)
                    JSQSystemSoundPlayer.jsq_playMessageSentSound()
                    self.finishSendingMessage()
                }
            }
        })
        
        task.resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employees.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        cell.textLabel?.text = employees[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedEmployee = employees[indexPath.row].name
    }
    
    
    override func textViewDidBeginEditing(_ textView: UITextView) {
        myMessageTextView = textView
        
        view.addConstraint(NSLayoutConstraint(item: employeeTableView, attribute: .bottom, relatedBy: .equal, toItem: textView, attribute: .top, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: employeeTableView, attribute: .width, relatedBy: .equal, toItem: textView, attribute: .width,multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: employeeTableView, attribute: .height, relatedBy: .equal, toItem: textView, attribute: .height,multiplier: 1, constant: 100))
        view.addConstraint(NSLayoutConstraint(item: employeeTableView, attribute: .left, relatedBy: .equal, toItem: textView, attribute: .left, multiplier: 1, constant: 0))
    }
    
   private func hlbBinarySearch(array: [String.Index], left: Int, right: Int, searchElement: String.Index) -> String.Index? {
        var size = array.count
        var mid = (left + right) / 2
        if (size == 1 || searchElement == array[mid]) {
            return nil
        }
        else if (searchElement > array[mid] && searchElement < array[mid + 1]) {
            return array[mid + 1]
        }
        else if (searchElement < array[mid] && searchElement > array[mid - 1]) {
            return array[mid]
        }
        else if (searchElement > array[mid]) {
            return hlbBinarySearch(array: array, left: mid + 1, right: right, searchElement: searchElement)
        }
        else if (searchElement < array[mid]) {
            return hlbBinarySearch(array: array, left: left, right: mid, searchElement: searchElement)
        }
        else {
            return nil
        }
    }
    
    private func lubBinarySearch(array: [String.Index], left: Int, right: Int, searchElement: String.Index) -> String.Index? {
        var size = array.count
        var mid = (left + right) / 2
        if (size == 1 || searchElement == array[mid]) {
            return nil
        }
        else if (searchElement > array[mid] && searchElement < array[mid + 1]) {
            return array[mid]
        }
        else if (searchElement < array[mid] && searchElement > array[mid - 1]) {
            return array[mid - 1]
        }
        else if (searchElement > array[mid]) {
            return lubBinarySearch(array: array, left: mid + 1, right: right, searchElement: searchElement)
        }
        else if (searchElement < array[mid]) {
            return lubBinarySearch(array: array, left: left, right: mid, searchElement: searchElement)
        }
        else {
            return nil
        }
    }
    
    override func textViewDidChange(_ textView: UITextView) {
        isTyping = textView.text != ""
        if (!textView.text.isEmpty) {
            let offset = textView.cursorOffset! - 1
            let lastCharacter = textView.text[textView.text.index(textView.text.startIndex, offsetBy: offset)]
            print("The string pointed by index is \(lastCharacter)")
            whenIgenerateSS = false
            super.textViewDidChange(textView)
            reloadEmployees()
            if let selectedRange = textView.selectedTextRange {
                
                    if (lastCharacter == "@") {
                        isEmployeeString = true
                        employeeTableView.isHidden = false
                        atIndices.append(textView.text.endIndex)
                        
                    }
                    else if (lastCharacter == " ") {
                        isEmployeeString = false
                        employeeTableView.isHidden = true
                    }
                    else {
                        if isEmployeeString {
                            print("Im at cursor detection")
                            if let cursorIndex = textView.cursorIndex {
                                print("Ive found the cursor index")
                                let range = textView.text.startIndex..<cursorIndex
                                let subString = textView.text.substring(with: range)
                                print(subString)
                                for (index, character) in subString.characters.enumerated().reversed() {
                                    if (character == " ") {
                                        print("No employee found in the cursorIndex pointing String")
                                        break
                                    }
                                    if (character == "@") {
                                        employeeTableView.reloadData()
                                        var indexOfCharacterNextToAt = subString.index(subString.startIndex, offsetBy: index + 1)
                                        var employeeRange = indexOfCharacterNextToAt..<cursorIndex
                                        self.employeeRange = employeeRange
                                        var employeeSubstring = subString.substring(with: employeeRange)
                                        whenIgenerateSS = true
                                        findSubStringsInEmployees(subString: employeeSubstring)
                                        break
                                    }
                                }
                            }
                        }
                    }
                
            }
    
        }
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "") {
            print("Backspace pressed")
            return true;
        }
        return true
    }
    
    func getRequest(url: URL) {
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [[[String: Any]]] {
                for selection in responseJSON {
                    if let responseJSON = selection as? [[String: Any]] {
                        print("Basic++")
                        for jsonDict in responseJSON {
                            print("Basic+++")
                            let userDict = jsonDict["user"] as! [String:String]
                            let name = userDict["username"] as! String
                            let email = userDict["email"] as! String
                            let employee = Employee(name: name, email: email)
                            self.employees.append(employee)
                            self.substituteArray.append(employee)
                        }
                        let employee = Employee(name: "phantom", email: "phantom")
                        self.employees.append(employee)
                        self.substituteArray.append(employee)
                    }
                }

            }
        })
        task.resume()
    }

    

}

extension UITextView {
    var cursorOffset: Int? {
        guard let range = selectedTextRange else { return nil }
        return offset(from: beginningOfDocument, to: range.start)
    }
    var cursorIndex: String.Index? {
        guard let cursorOffset = cursorOffset else { return nil }
        return text.index(text.startIndex, offsetBy: cursorOffset)
    }
}

extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
}

extension String {
    func index(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    func endIndex(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    func indexes(of string: String, options: CompareOptions = .literal) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.upperBound
        }
        return result
    }
    func ranges(of string: String, options: CompareOptions = .literal) -> Bool {
        var result: [Range<Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            return true
        }
        return false
    }
}


