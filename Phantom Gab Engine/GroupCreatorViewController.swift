//
//  GroupCreatorViewController.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 7/3/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//

import Firebase
import UIKit


class GroupCreatorViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var channelName: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    //@IBOutlet weak var channelName: UITextField!
    var selectedEmployees: [RolesTableViewController.SelectedEmployee]?
    var selectedTasks: [TasksAdditionViewControllerTableViewController.Section]?
    var taskList: [String] = []
    var taskListDict = [String : Any]()
    let taskSubmissionURL = URL(string: "http://localhost:8000/phantom/submitTasks")
    let channelListController: ChannelListViewController = ChannelListViewController()
    var apiSubmissionDict: [String : Any]?
    
    
    //@IBOutlet weak var imageView: UIImageView!
    var imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        imageView.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(loadImageButtonTapped(tapGestureRecognizer:)))
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc private func loadImageButtonTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let xmin = self.imageView.frame.minX
            let ymin = self.imageView.frame.minY
            let frame = CGRect(x: xmin, y: ymin, width: 100, height: 100)
            self.imageView.frame = frame
            self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2
            self.imageView.clipsToBounds = true
            let width = imageView.bounds.width
            let height = imageView.bounds.height
            let size = CGSize(width: width, height: height)
            let properImage = scaleUIImageToSize(image: pickedImage, size: size)
            imageView.image = properImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        dismiss(animated: true, completion: nil)
    }
    
    func abc() {
        print(selectedEmployees)
        print(selectedTasks)
    }
    
    func scaleUIImageToSize( image: UIImage, size: CGSize) -> UIImage {
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Im here")
    }
    
    func submitToApi(url: URL, jsonData: Data) {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            print("Ive communicated with the API")
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
            
            }
        })
        
        task.resume()
    }
    
    private func convertToJSON(toConvertData: Any) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: toConvertData, options: .prettyPrinted)
            print(jsonData)
            submitToApi(url: taskSubmissionURL!, jsonData: jsonData)
        }
        catch {
            print ("Error converting to json")
        }
        
    }
    
    

    
    @IBAction func unwindToGroupCreatorViewController(segue: UIStoryboardSegue) {
        
        let alert = UIAlertController(title: "Create Channel",
                                      message: "Can we save your preferences?",
                                      preferredStyle: .alert)
        
        let createAction = UIAlertAction(title: "Create",
                                         style: .default) { action in
            if (self.channelName.text != nil) {
                var employeeList: [String] = []
                let channel_image = self.imageView.image
                var data = Data()
                data = UIImageJPEGRepresentation(channel_image!, 0.8)!
                if self.selectedEmployees != nil {
                    for employee in self.selectedEmployees! {
                        let email = employee.item["email"]
                        if email != nil {
                            employeeList.append(email!)
                        }
                    }
                }
                self.channelListController.addChannel(channelName: self.channelName.text!, channelImage: data, employees: employeeList)
                self.navigationController?.popViewController(animated: true)
                var tasks: [[String:Any]] = []
                for section in self.selectedTasks! {
                    var taskDict: [String:Any] = [:]
                    taskDict["task_name"] = section.name
                    taskDict["sub_tasks"] = section.items
                    tasks.append(taskDict)
                }
                var employees: [[String: Any]] = []
                for selectedEmployee in self.selectedEmployees! {
                    var employeeDict: [String:Any] = [:]
                    employeeDict["role_name"] = selectedEmployee.role_name
                    employeeDict["employee"] = selectedEmployee.item
                    employees.append(employeeDict)
                }
                var jsonSubmit: [String : Any ]  = [:]
                var signedInUserEmail = Auth.auth().currentUser?.email
                if (signedInUserEmail != nil) {
                    jsonSubmit = ["tasks" : tasks, "employees" : employees, "channel_name": self.channelName.text!, "manager_email": signedInUserEmail!]
                    print(jsonSubmit)
                    self.convertToJSON(toConvertData: jsonSubmit)
                }
            }
            
        }
        
        let dismissAction = UIAlertAction(title: "Cancel",
                                         style: .default) { action in
           alert.dismiss(animated: true, completion: nil)
        }
        
        
        alert.addAction(createAction)
        alert.addAction(dismissAction)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

