//
//  LinksTableViewController.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 29/07/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//

import UIKit

class LinksTableViewController: UITableViewController {

    let linkRequestURL: URL = URL(string: "http://localhost:8000/phantom/listLinks/hackathon")!
    var linkList: [[String : String]] = [[:]]
    let urlTapRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapFunction:"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getRequest(url: linkRequestURL)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return linkList.count
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "showLink")
        let linkDict = linkList[indexPath.row]
        cell?.textLabel?.text = linkDict["linkName"]
        let urlString = linkDict["url"]
        cell?.detailTextLabel?.text = urlString
        cell?.detailTextLabel?.isUserInteractionEnabled = true
        cell?.detailTextLabel?.addGestureRecognizer(urlTapRecognizer)
        return cell!
    }
    
    func getRequest(url: URL) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            print("Ive communicated with the API")
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            
            if let responseJSON = responseJSON as? [[String: String]] {
                for jsonDict in responseJSON {
                    let linkName = jsonDict["link_name"] as! String
                    let url = jsonDict["url"] as! String
                    let linkDict = ["linkName" : linkName, "url" : url]
                    print(linkDict)
                    self.linkList.append(linkDict)
                    self.tableView.reloadData()
                }
            }
        })
        task.resume()
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let linkDict = linkList[indexPath.row]
        let linkURL = linkDict["url"]
        let url = URL(string: linkURL!)!
        print(url)
        UIApplication.shared.open(url, options: [:])
        
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
