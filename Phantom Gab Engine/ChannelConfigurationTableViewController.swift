//
//  ChannelConfigurationTableViewController.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 6/15/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//
import Firebase
import JSQMessagesViewController
import UIKit

class ChannelConfigurationTableViewController: UITableViewController {
    
    var channelName: String?
    var chatControllerInstance = ChatViewController()
    var taskList: [String] = []
    var taskListDict = [String : Any]()
    let taskSubmissionURL = URL(string: "https://nameless-meadow-44269.herokuapp.com/phantom/submitTasks")
    let messageRequestIdentifer = 101
    let taskRequestIdentifier = 11
    
    @IBAction func onDonePress(_ sender: UIBarButtonItem) {
        taskListDict["project_name"] = channelName
        taskListDict["tasks"] = taskList
        taskListDict["manager_email"] = Auth.auth().currentUser?.email
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: taskListDict, options: .prettyPrinted)
            print ("The json form is \(jsonData)")
            submitToApi(url: taskSubmissionURL!, jsonData: jsonData)
            
        }
        catch {
           print ("Error converting to json")
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Add Task",
            message: "Please personalize your channel by adding a task",
            preferredStyle: .alert)
        alert.addTextField { taskName in
            taskName.placeholder = "Enter task name"
        }
        let createAction = UIAlertAction(title: "Create", style: .default) {
            action in
            let taskNameTextField = alert.textFields![0]
            if let taskName = taskNameTextField.text {
                self.taskList.append(taskName)
                self.tableView.reloadData()
            }
            
        }
        alert.addAction(createAction)
        present(alert, animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var arrayIndex = indexPath.row
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskName", for: indexPath)
        cell.textLabel?.text = taskList[arrayIndex]
        return cell
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func submitToApi(url: URL, jsonData: Data) {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            print("Ive communicated with the API")
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                
            }
        })
        
        task.resume()
    }

}
