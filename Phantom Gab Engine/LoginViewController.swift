//
//  MainViewController.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 5/29/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//
import Firebase
import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func loginDidTouch(_ sender: UIButton) {
        signInUserAnonymously()
    }
    
    internal func signInUserAnonymously() {
        if nameTextField?.text != "" {
            Auth.auth().signInAnonymously(completion: {
                (user, error) in
                if let err = error {
                    print (err.localizedDescription)
                    return
                }
                self.performSegue(withIdentifier: "goToChannels", sender: nil)
            })
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        let navVc = segue.destination as! UINavigationController
        let channelVc = navVc.viewControllers.first as! ChannelListViewController
        channelVc.senderDisplayName = nameTextField!.text
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("Sricharan at Return")
        nameTextField.resignFirstResponder()
        signInUserAnonymously()
        return true
    }
}
