//
//  ChannelListCell.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 21/07/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//

import UIKit

class ChannelListCell: UITableViewCell {

   @IBOutlet weak var channelName: UILabel!
   
    @IBOutlet weak var imView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
