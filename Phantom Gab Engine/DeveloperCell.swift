//
//  DeveloperCell.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 10/07/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//

import UIKit

class DeveloperCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var magnitudeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
