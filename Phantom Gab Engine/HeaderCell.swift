//
//  HeaderCell.swift
//  Phantom Gab Engine
//
//  Created by Sricharan on 08/07/17.
//  Copyright © 2017 Rabbit Inc. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

   
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var toggleButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization cod   e
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
